[![Rawsec's CyberSecurity Inventory](https://inventory.rawsec.ml/img/badges/Rawsec-inventoried-FF5050_for-the-badge.svg)](https://inventory.rawsec.ml/)

# ANDRAX Hackers Platform
ANDRAX The most Complete and Advanced Ethical Hacking and Penetration Testing Platform, developed for Android and general ARM devices

# How to install

Download ANDRAX from our [official site](http://andrax.thecrackertechnology.com)
